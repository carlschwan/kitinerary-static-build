#!/bin/sh

mkdir -p output
mkdir -p build

docker run -it --rm \
    -v `pwd`/config:/home/builder/config \
    -v `pwd`/build:/home/builder/build \
    -v `pwd`/output:/home/builder/output \
    vkrause/kde-static-builder build-static-module.sh $1
