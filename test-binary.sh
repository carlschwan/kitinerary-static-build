#!/bin/sh

image=$1
shift
executable=$1
shift

if [ -z "$image" ] || [ -z "$executable" ]; then
    echo "Usage $0 <image> <executable> [args]"
    echo "Example: $0 debian:stable kitinerary-extractor --capabilities"
    exit 1
fi

docker run -it --rm -v `pwd`/output:/output $image "/output/$executable" $@
