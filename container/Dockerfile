FROM opensuse/leap:42.3

# install build dependencies
RUN zypper --non-interactive install \
    autoconf \
    automake \
    bash-completion \
    cmake \
    gcc7 \
    gcc7-c++ \
    gettext-runtime \
    gettext-tools \
    git \
    glibc-devel-static \
    gperf \
    libtool \
    make \
    perl \
    perl-IO-Socket-SSL \
    perl-YAML-Syck \
    python3 \
    ruby2.4 \
    shared-mime-info \
    subversion \
    vim \
    which \
    zlib-devel-static

RUN useradd -u 1000 -g 100 -m builder

# gcc-7 is a non-standard compiler on this platform, hide that
RUN ln -s /usr/bin/gcc-7 /usr/bin/gcc && \
    ln -s /usr/bin/g++-7 /usr/bin/g++ && \
    ln -s /usr/bin/gcc-ar-7 /usr/bin/gcc-ar && \
    ln -s /usr/bin/gcc-nm-7 /usr/bin/gcc-nm && \
    ln -s /usr/bin/gcc-ranlib-7 /usr/bin/gcc-ranlib && \
    ln -s /usr/bin/gcov-7 /usr/bin/gcov && \
    ln -s /usr/bin/gcov-dump-7 /usr/bin/gcov-dump && \
    ln -s /usr/bin/gcov-tool-7 /usr/bin/gcov-tool  && \
    ln -s /usr/bin/cpp-7 /usr/bin/cpp && \
    ln -s /usr/bin/gcc /usr/bin/cc && \
    ln -s /usr/bin/g++ /usr/bin/c++

# same for ruby
RUN rm -f /usr/bin/ruby && ln -s /usr/bin/ruby.ruby2.4 /usr/bin/ruby

# install kdesrc-build
RUN mkdir /tmp/kdesrc-build && \
    cd /tmp/kdesrc-build && \
    git clone https://invent.kde.org/kde/kdesrc-build.git && \
    cd kdesrc-build && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_INSTALL_PREFIX=/usr .. && \
    make && \
    make install
    
# install a statically built freetype, needed for Qt and poppler
RUN mkdir /tmp/freetype && \
    cd /tmp/freetype && \
    git clone git://git.sv.nongnu.org/freetype/freetype2.git && \
    cd freetype2 && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_INSTALL_PREFIX=/usr -DBUILD_TESTING=OFF -DBUILD_SHARED_LIBS=OFF .. && \
    make && \
    make install

COPY content/*.sh /usr/bin/

USER builder
ENV LANG=en_US.UTF-8
# force UTF-8 for ruby, otherwise this old setup still defaults to US-ASCII and chokes on the PO files
ENV RUBYOPT="-KU -E utf-8:utf-8"
WORKDIR /home/builder
CMD /bin/bash
